#!/bin/bash -ex

# CONFIG
prefix="FileZilla"
suffix=""
munki_package_name="FileZilla"
display_name="FileZilla"
icon_name=""
category="Utilities"
description="This update contains stability and security fixes for the FileZilla FTP Client"
 
url=`./finder.sh`
url_arm=`./finder-arm.sh`

# download it (-L: follow redirects)
curl -L -o FileZilla* "${url}"

# Archive is a tar.bzip2 file now. Yay for another change.
tar -xjvf FileZilla*
#unzip FileZilla*

# clean up temp file
rm -rf "FileZilla*"

# make DMG from the inner prefpane
mkdir -p build-root/Applications
mv FileZilla.app build-root/Applications
rm -rf "FileZiolla.app"

# download it (-L: follow redirects)
curl -L -o FileZilla* "${url_arm}"

# Archive is a tar.bzip2 file now. Yay for another change.
tar -xjvf FileZilla*
#unzip FileZilla*

# clean up temp file
rm -rf "FileZilla*"

# make DMG from the inner prefpane
mkdir -p build-root-arm/Applications
mv FileZilla.app build-root-arm/Applications
rm -rf "FileZiolla.app"
#hdiutil create -srcfolder app_tmp -format UDZO -o app.dmg


version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" build-root/Applications/*.app/Contents/Info.plist`
version_arm=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" build-root-arm/Applications/*.app/Contents/Info.plist`


#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

#clean up the component file
rm -rf Component-${munki_package_name}.plist

## ARM
/usr/bin/pkgbuild --analyze --root build-root-arm/ Component-${munki_package_name}arm.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}arm.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}arm.plist

#build PKG
/usr/bin/pkgbuild --root build-root-arm/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}arm.plist --version ${version_arm} app_arm.pkg

#clean up the component file
rm -rf Component-${munki_package_name}arm.plist

## Create pkg's
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg "${key_files}" | /bin/bash > app.plist

echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app_arm.pkg "${key_files}" | /bin/bash > app_arm.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
perl -p -i -e 's/build-root//' app_arm.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist
plist_arm=`pwd`/app_arm.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.15.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
#defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Change path and other details in the plist
defaults write "${plist_arm}" installer_item_location "jenkins/${prefix}-${version}-arm.pkg"
defaults write "${plist_arm}" minimum_os_version "10.15.0"
defaults write "${plist_arm}" uninstallable -bool NO
defaults write "${plist_arm}" name "${munki_package_name}"
defaults write "${plist_arm}" icon_name "${icon_name}"
#defaults write "${plist}" version "${version}"
defaults write "${plist_arm}" supported_architectures -array arm64
defaults write "${plist_arm}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"
defaults write "${plist_arm}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"
defaults write "${plist_arm}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"
defaults write "${plist_arm}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Make readable by humans for ARM
/usr/bin/plutil -convert xml1 "${plist_arm}"
chmod a+r "${plist_arm}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app_arm.pkg   ${prefix}-${version}-arm.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
mv app_arm.plist ${prefix}-${version}-arm.plist

$HOME/jenkins-trello/otto-notify "${munki_package_name}" "${version}" "${prefix}-${version}.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}"  $*
$HOME/jenkins-trello/otto-notify "${munki_package_name}arm" "${version_arm}" "${prefix}-${version_arm}-arm.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}"  $*
