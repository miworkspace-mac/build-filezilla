#!/bin/bash -ex

URL=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o FileZilla* "${URL}"

# Archive is a tar.bzip2 file now. Yay for another change.
tar -xjvf FileZilla*
#unzip FileZilla*

# clean up temp file
rm -rf "FileZilla*"

# make DMG from the inner prefpane
mkdir -p build-root/Applications
mv FileZilla.app build-root/Applications
rm -rf "FileZiolla.app"
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" build-root/Applications/*.app/Contents/Info.plist`

rm -rf build-root

echo $VERSION

if [ "x${VERSION}" != "x" ]; then
    echo version: "${VERSION}"
    echo "${VERSION}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi